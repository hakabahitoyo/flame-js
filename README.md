# flame.js

A port/clone of _flame.el_, from Emacs Lisp to JavaScript.

Try now: https://hakabahitoyo.gitlab.io/flame-js/.

See also: http://www.splode.com/~friedman/software/emacs-lisp/src/flame.el.
